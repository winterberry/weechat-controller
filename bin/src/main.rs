use either::Either;
use secrecy::SecretString;
use tracing::{info, instrument};
use weechat_controller::{
    controller::controller_main_loop,
    controller_config::ControllerConfig,
    redis::{Redis, RedisConfig, RedisError},
    relay_error::RelayError,
    weechat_relay::{WeechatRelay, WeechatRelayConfig},
};

/// Environment variable getter with clearer error description.
fn get_env_var(var_name: &'static str) -> Result<String, Error> {
    match std::env::var(var_name) {
        Err(_) => {
            tracing::error!("Failed to load environment variable: {var_name:?}");
            Err(Error::MissingEnvVar(var_name))
        }
        Ok(value) => Ok(value),
    }
}

// #lizard forgives the complexity
#[instrument]
fn get_config_from_env_vars() -> Result<ControllerConfig<WeechatRelay, Redis>, Error> {
    let redis_hostname = get_env_var("REDIS_HOSTNAME")?;
    let redis_port = get_env_var("REDIS_PORT")?;
    let redis_password = SecretString::new(get_env_var("REDIS_PASSWORD")?);
    let redis_set_name = get_env_var("REDIS_SET_NAME")?;
    let weechat_relay_hostname = get_env_var("WEECHAT_RELAY_HOSTNAME")?;
    let weechat_relay_port = get_env_var("WEECHAT_RELAY_PORT")?;
    let weechat_relay_password = SecretString::new(get_env_var("WEECHAT_RELAY_PASSWORD")?);

    let controller_config: ControllerConfig<WeechatRelay, Redis> = ControllerConfig {
        monitor_config: WeechatRelayConfig {
            weechat_relay_host: format!("{weechat_relay_hostname}:{weechat_relay_port}"),
            weechat_relay_password,
        },
        broker_config: RedisConfig {
            redis_host: format!("{redis_hostname}:{redis_port}"),
            redis_password,
            redis_set_name,
        },
    };

    Ok(controller_config)
}

#[tokio::main]
#[instrument]
async fn main() -> Result<(), Error> {
    // TODO: Set default log level to info
    tracing_subscriber::fmt::init();
    let controller_config = get_config_from_env_vars()?;
    info!(
        "Starting program with configuration: {:?}",
        controller_config
    );
    controller_main_loop(controller_config).await?;

    Ok(())
}

#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
enum Error {
    MissingEnvVar(&'static str),
    RelayError(RelayError),
    RedisError(RedisError),
}

impl From<RelayError> for Error {
    fn from(err: RelayError) -> Self {
        Error::RelayError(err)
    }
}

impl From<RedisError> for Error {
    fn from(err: RedisError) -> Self {
        Error::RedisError(err)
    }
}

impl From<Either<RelayError, RedisError>> for Error {
    fn from(err: Either<RelayError, RedisError>) -> Self {
        match err {
            Either::Left(relay_err) => Error::RelayError(relay_err),
            Either::Right(redis_err) => Error::RedisError(redis_err),
        }
    }
}

#[cfg(test)]
mod tests {
    use secrecy::ExposeSecret;

    use super::*;
    use std::env;

    #[test]
    fn test_get_config_from_env_vars() {
        // Setup
        env::set_var("REDIS_HOSTNAME", "localhost");
        env::set_var("REDIS_PORT", "6379");
        env::set_var("REDIS_PASSWORD", "password");
        env::set_var("REDIS_SET_NAME", "set");
        env::set_var("WEECHAT_RELAY_HOSTNAME", "localhost");
        env::set_var("WEECHAT_RELAY_PORT", "9001");
        env::set_var("WEECHAT_RELAY_PASSWORD", "password");

        // Test
        let config = get_config_from_env_vars().unwrap();

        // Check correctness
        assert_eq!(config.broker_config.redis_host, "localhost:6379");
        assert_eq!(
            config.broker_config.redis_password.expose_secret(),
            "password"
        );
        assert_eq!(config.broker_config.redis_set_name, "set");
        assert_eq!(config.monitor_config.weechat_relay_host, "localhost:9001");
        assert_eq!(
            config.monitor_config.weechat_relay_password.expose_secret(),
            "password"
        );

        // Teardown
        env::remove_var("REDIS_HOSTNAME");
        env::remove_var("REDIS_PORT");
        env::remove_var("REDIS_PASSWORD");
        env::remove_var("REDIS_SET_NAME");
        env::remove_var("WEECHAT_RELAY_HOSTNAME");
        env::remove_var("WEECHAT_RELAY_PORT");
        env::remove_var("WEECHAT_RELAY_PASSWORD");
    }
}
