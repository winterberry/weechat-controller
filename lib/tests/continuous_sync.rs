use map_macro::hash_set;
use std::time::Duration;
use tokio::time::sleep;
use tracing::info;
use weechat_controller::controller_config::ControllerConfig;
use weechat_controller::redis::Redis;
use weechat_controller::test_helpers::{establish_redis_test_conn, establish_relay_test_conn};
use weechat_controller::weechat_relay::WeechatRelay;
use weechat_controller::TINY;

/// Test if synchronization works while the program is running
// #lizard forgives the complexity
#[rstest::rstest]
#[test_log::test(tokio::test)]
#[timeout(Duration::from_secs(15))]
async fn continuous_sync() -> Result<(), Box<dyn std::error::Error>> {
    let mut relay = establish_relay_test_conn().await;
    let mut redis = establish_redis_test_conn().await;

    relay.part_all_twitch_channels()?;
    while !relay.get_open_twitch_channels()?.is_empty() {
        sleep(TINY).await;
    }

    redis.clear_channel_set()?;

    let test_config = ControllerConfig::<WeechatRelay, Redis>::load_test_config().await;
    tokio::spawn(async {
        info!("Starting main loop");
        weechat_controller::controller::controller_main_loop(test_config)
            .await
            .unwrap()
    });

    // wait for controller to be ready
    sleep(Duration::from_millis(2000)).await;

    redis.add_channel("#twitch")?;
    redis.add_channel("#xqc")?;
    while relay.get_open_twitch_channels()? != hash_set! {"#twitch".to_string(), "#xqc".to_string()}
    {
        sleep(TINY).await;
    }

    redis.remove_channel("#xqc")?;
    while relay.get_open_twitch_channels()? != hash_set! {"#twitch".to_string()} {
        sleep(TINY).await;
    }

    Ok(())
}
