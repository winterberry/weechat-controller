use map_macro::hash_set;
use std::time::Duration;
use tokio::time::sleep;
use tracing::info;
use weechat_controller::controller_config::ControllerConfig;
use weechat_controller::redis::Redis;
use weechat_controller::test_helpers::{establish_redis_test_conn, establish_relay_test_conn};
use weechat_controller::weechat_relay::WeechatRelay;
use weechat_controller::TINY;

/// Test if synchronization works on startup
// #lizard forgives the complexity
#[rstest::rstest]
#[test_log::test(tokio::test)]
#[timeout(Duration::from_secs(15))]
async fn initial_sync() -> Result<(), Box<dyn std::error::Error>> {
    let mut relay = establish_relay_test_conn().await;
    let mut redis = establish_redis_test_conn().await;

    relay.part_all_twitch_channels()?;
    while !relay.get_open_twitch_channels()?.is_empty() {
        sleep(TINY).await;
    }

    redis.clear_channel_set()?;
    redis.add_channel("#twitch")?;

    let test_config = ControllerConfig::<WeechatRelay, Redis>::load_test_config().await;
    tokio::spawn(async move {
        info!("Starting main loop");
        weechat_controller::controller::controller_main_loop(test_config)
            .await
            .unwrap()
    });

    // wait for controller to be launched
    sleep(Duration::from_millis(2000)).await;

    let open_channels = relay.get_open_twitch_channels()?;
    println!("Open Channels: {open_channels:?}");
    assert_eq!(open_channels, hash_set!["#twitch".to_string()]);

    Ok(())
}
