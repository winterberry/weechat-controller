//! A structure representing a configuration for a controller that includes a chat monitor and a broker.
use std::fmt::Debug;

use crate::controller::{AbstractControllerConfig, ChatMonitor, MsgBroker};

/// A structure representing a configuration for a controller that includes a chat monitor and a broker.
#[derive(Debug)]
pub struct ControllerConfig<CM, MB>
where
    CM: ChatMonitor,
    CM::Config: Debug,
    MB: MsgBroker,
    MB::Config: Debug,
{
    /// Configuration for the chat monitor.
    pub monitor_config: CM::Config,
    /// Configuration for the message broker.
    pub broker_config: MB::Config,
}

impl<CM, MB> AbstractControllerConfig<CM, MB> for ControllerConfig<CM, MB>
where
    CM: ChatMonitor,
    CM::Config: Debug,
    MB: MsgBroker,
    MB::Config: Debug,
{
    fn monitor_config(&self) -> &CM::Config {
        &self.monitor_config
    }

    fn broker_config(&self) -> &MB::Config {
        &self.broker_config
    }
}
