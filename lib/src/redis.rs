//! This module allows us to use Redis as a Message Broker in the controller.

pub use redis::RedisError;

use std::collections::HashSet;

use crate::controller::{BrokerConfig, BrokerConnection, MsgBroker};
use futures_util::StreamExt;
use redis::Msg;
use secrecy::SecretString;
use std::fmt;
use tracing::{debug, info, instrument};

/// Configuration to access and authenticate to the Redis message broker.
#[derive(Clone, Debug)]
pub struct RedisConfig {
    /// Host under which the Redis install can be accessed.
    pub redis_host: String,
    /// Password to authenticate with redis.
    pub redis_password: SecretString,
    /// The name of the set that contains the names of the Twitch channels which we are tracking.
    pub redis_set_name: String,
}

/// Represents a connection to Redis. We use `PubSub` to receive information about whether there are updates to the channel set, but then have to get the members of of the set using a SMEMBERS command.
/// Since keeping the `PubSub` channel open locks the redis Connection, we need two separate connections, one to listen to `PubSub` notifications, and one to make requests to Redis.
#[allow(missing_debug_implementations)]
pub struct RedisConnection {
    /// Connection used to make requests to Redis.
    pub request_connection: redis::Connection,
    /// Connection used to listen to PubSub notifications.
    pub pubsub_connection: redis::aio::PubSub,
}

/// The main Redis message broker structure.
pub struct Redis {
    /// Connection to the Redis Database.
    pub connection: RedisConnection,
    /// Configuration to authorize and reach the Redis data.
    pub config: RedisConfig,
}

impl BrokerConfig for RedisConfig {}

impl BrokerConnection for RedisConnection {}

impl RedisConfig {
    /// Encode the special characters within a password to be valid URL characters
    fn url_encode(s: impl AsRef<str>) -> String {
        url::form_urlencoded::byte_serialize(s.as_ref().as_bytes()).collect::<String>()
    }

    #[must_use]
    /// Create a redis address with authorization info, based on the configuration information.
    pub fn redis_auth_address(&self) -> SecretString {
        use secrecy::ExposeSecret;
        let encoded_password = Self::url_encode(self.redis_password.expose_secret().clone());
        let auth_address = format!("redis://:{encoded_password}@{}", self.redis_host);

        SecretString::new(auth_address)
    }
}

#[async_trait::async_trait]
impl MsgBroker for Redis {
    type Config = RedisConfig;
    type Connection = RedisConnection;
    type Message = redis::Msg;
    type Error = redis::RedisError;

    #[instrument]
    async fn connect(config: &Self::Config) -> Result<Self, Self::Error> {
        use secrecy::ExposeSecret;
        let client = redis::Client::open(config.redis_auth_address().expose_secret().clone())?;
        let request_connection = client.get_connection()?;
        info!("Redis Request connection established");

        let pubsub_client =
            redis::Client::open(config.redis_auth_address().expose_secret().clone())?;
        let pubsub_connection = pubsub_client.get_async_connection().await?.into_pubsub();
        info!("Redis PubSub connection established");

        let connection = RedisConnection {
            request_connection,
            pubsub_connection,
        };

        Ok(Self {
            config: config.clone(),
            connection,
        })
    }

    /// Subscribe to the appropriate Redis PubSub channel.
    async fn subscribe_to_notifications(&mut self) -> Result<(), Self::Error> {
        let pubsub_channel_name = format!("__keyspace@0__:{}", self.config.redis_set_name);
        self.connection
            .pubsub_connection
            .subscribe(pubsub_channel_name)
            .await
    }

    /// This function subscribes to the appropriate PubSub channel and then waits until
    async fn notification_stream(&mut self) -> Option<Msg> {
        self.connection.pubsub_connection.on_message().next().await
    }

    #[allow(clippy::cognitive_complexity)]
    fn get_channels(&mut self) -> HashSet<String> {
        let tracked_channels_set_name = self.config.redis_set_name.clone();

        let channels = redis::cmd("SMEMBERS")
            .arg(tracked_channels_set_name)
            .query::<HashSet<String>>(&mut self.connection.request_connection)
            .unwrap_or_default();

        debug!("List of channels retrieved from Redis: {channels:?}");
        channels
    }
}

impl fmt::Debug for Redis {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Redis")
            .field("config", &self.config)
            .finish_non_exhaustive()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::test_helpers::establish_redis_test_conn;

    use super::*;
    #[rstest::rstest]
    #[test_log::test(tokio::test)]
    #[timeout(Duration::from_secs(15))]
    async fn receive_notification() {
        let mut redis_broker = establish_redis_test_conn().await;
        info!("Established Redis Connection");

        // A notification is only actually received if the added channel is new, so we clear the set to make sure
        redis_broker.clear_channel_set().unwrap();

        redis_broker.subscribe_to_notifications().await.unwrap();
        info!("Subscribed to notifications");

        // Add an element to the redis set
        redis_broker.add_channel("sample_channel").unwrap();
        info!("Added channel to set");

        // Test if a notification is received
        redis_broker.notification_stream().await;
        info!("Notification received");
    }
}
