//! An enumeration of all possible errors that can occur during the use of [`WeechatRelay`].
use std::error::Error;
use std::io;
use weechat_relay_rs::commands::WeechatError;
use weechat_relay_rs::message_parser::ParseMessageError;

/// An enumeration of all possible errors that can occur during the use of [`WeechatRelay`].
pub enum RelayError {
    /// Represents a standard I/O error.
    IoError(io::Error),
    /// Represents an error that can occur when interacting with Weechat.
    WeechatError(WeechatError),
    /// Represents an error that can occur when decoding HData.
    DecodeHDataError,
    /// Represents an error that can occur when parsing a message.
    ParseMessageError(ParseMessageError<Vec<u8>, nom::error::VerboseError<Vec<u8>>>),
    /// Represents an error that can occur when converting a string from UTF-8.
    FromUtf8Error(std::string::FromUtf8Error),
}

impl From<io::Error> for RelayError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

impl From<WeechatError> for RelayError {
    fn from(err: WeechatError) -> Self {
        Self::WeechatError(err)
    }
}

impl From<ParseMessageError<Vec<u8>, nom::error::VerboseError<Vec<u8>>>> for RelayError {
    fn from(err: ParseMessageError<Vec<u8>, nom::error::VerboseError<Vec<u8>>>) -> Self {
        Self::ParseMessageError(err)
    }
}

impl From<std::string::FromUtf8Error> for RelayError {
    fn from(err: std::string::FromUtf8Error) -> Self {
        Self::FromUtf8Error(err)
    }
}

impl std::fmt::Display for RelayError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IoError(err) => write!(f, "IO Error: {err}"),
            Self::WeechatError(_) => write!(f, "New line in argument"),
            Self::DecodeHDataError => write!(
                f,
                "Received HData response that did not have the expected format"
            ),
            Self::ParseMessageError(err) => write!(f, "Parse Message Error: {err:?}"),
            Self::FromUtf8Error(err) => write!(f, "UTF8 Conversion Error: {err}"),
        }
    }
}

impl std::fmt::Debug for RelayError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self}")
    }
}

impl Error for RelayError {}
