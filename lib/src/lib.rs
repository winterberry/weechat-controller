//! # weechat-controller
//! A controller that reads channel information from a Message Broker (Redis) and updates a Chat Monitor (WeeChat) whenever the list of tracked channels is updated.
//!
//! ## Required Environment:
//! This controller expects a Weechat and Redis to be installed, and both to be accessible to the controller.
//! The following settings in Weechat need to be set for this program to work correctly:
//! - /server add twitch irc.chat.twitch.tv -tls -password=$(TWITCH_OAUTH) -nicks=$(TWITCH_USERNAME) -username=$(TWITCH_USERNAME)
//! - /set irc.server.twitch.capabilities "twitch.tv/tags,twitch.tv/commands,twitch.tv/membership"
//! - /set irc.server.twitch.autoconnect on
//! - /set irc.look.part_closes_buffer on;
//! - /set relay.network.password $(RELAY_PASSWORD);
//! - /set irc.server.twitch.anti_flood_prio_low 0;
//! - /set irc.server.twitch.anti_flood_prio_high 0;
//! - /relay add weechat $(WEECHAT_PORT)
//!
//! ## Limitations:
//! - Redis PubSub doesn't actually tell us which channels are added or left, instead we receive a notification that "something has changed", and then we have to load the entire set of tracked channels and compare it with which channels are currently joined. This probably causes quite a performance hit.
//! - The WeeChat Relay connection is blocking, whereas the Redis connection is async.

#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![warn(
    warnings,
    future_incompatible,
    nonstandard_style,
    rust_2018_compatibility,
    rust_2018_idioms,
    unused,
    missing_docs,
    missing_copy_implementations,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unused_crate_dependencies,
    unused_extern_crates,
    variant_size_differences
)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::wildcard_imports)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::doc_markdown)]

pub mod controller;
pub mod controller_config;
pub mod redis;
pub mod relay_error;
pub mod weechat_relay;

#[cfg(feature = "test-helpers")]
pub mod test_helpers;

#[cfg(feature = "test-helpers")]
pub mod test_helpers_kube;

#[cfg(test)]
use {toml_edit as _, tracing_subscriber as _, weechat_controller as _};

#[cfg(feature = "test-helpers")]
/// A very short amount of time
pub const TINY: std::time::Duration = std::time::Duration::from_millis(10);
