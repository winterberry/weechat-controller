//! Functionality that helps retrieve information from a Kubernetes cluster.
//! If you have a test setup on a Kubernetes setup, you can use this to load addresses and passwords straight from the cluster rather than having to store them as constants or configuration files.
//!
//! This module will probably be moved into a separate crate at some point.

use std::{fmt::Debug, string::FromUtf8Error};

use k8s_openapi::{
    api::{
        apps::v1::Deployment,
        core::v1::{Container, EnvVar, Secret, SecretKeySelector, Service},
    },
    serde::de::DeserializeOwned,
};
use kube::{
    api::{Api, ListParams},
    core::Resource,
    Client, Config,
};
use secrecy::SecretString;

/// A function that searches for a Kubernetes object by its labels.
/// You can specify the type of object by providing the an appropriate type for `K`.
/// The function returns the first object that has the provided labels, if one exists.
pub async fn search_by_labels<K>(
    client: &Client,
    labels: impl AsRef<str> + Send,
) -> Result<K, Error>
where
    K: Resource + Clone + DeserializeOwned + Debug,
    K::DynamicType: Default,
{
    let api: Api<K> = Api::all(client.clone());
    let lp = ListParams::default().labels(labels.as_ref());
    api.list(&lp)
        .await?
        .into_iter()
        .next()
        .ok_or(Error::SearchFailed)
}

/// Retrieves all containers in a given deployment. This includes both regular containers and init containers.
pub fn get_all_containers_in_deployment(deployment: &Deployment) -> Result<Vec<&Container>, Error> {
    let pod_spec = deployment
        .spec
        .as_ref()
        .ok_or(Error::FieldNotFound(KubeFields::DeploymentSpec))?
        .template
        .spec
        .as_ref()
        .ok_or(Error::FieldNotFound(KubeFields::PodSpec))?;

    let containers = &pod_spec.containers;
    let init_containers = pod_spec.init_containers.iter().flatten();
    Ok(containers.iter().chain(init_containers).collect())
}

/// Loads a value from a Kubernetes Secret. If found, the value is returned wrapped in a [`SecretString`].
pub async fn load_value_from_secret(
    client: &Client,
    secret_namespace: impl AsRef<str> + Send + Sync,
    secret_name: impl AsRef<str> + Send + Sync,
    secret_key: impl AsRef<str> + Send + Sync,
) -> Result<SecretString, Error> {
    let secret = {
        let secrets: Api<Secret> = Api::namespaced(client.clone(), secret_namespace.as_ref());
        secrets.get(secret_name.as_ref()).await?
    };

    let secret_data = secret.data.ok_or(Error::DataNotFound)?;
    let raw_value = secret_data[secret_key.as_ref()].clone();
    let value = String::from_utf8(raw_value.0)?;
    Ok(SecretString::new(value))
}

/// Within a Deployment definition, try to find the [`SecretKeySelector`] that refers to an environment variable fulfilling a given condition.
/// e.g.
/// ```no_run
/// use weechat_controller::test_helpers_kube::*;
/// use kube::{Client, Config};
/// use k8s_openapi::api::apps::v1::Deployment;
/// async {
///     let sample_config = Config::infer().await.unwrap();
///     let kube_client = Client::try_from(sample_config).unwrap();
///     let deployment = search_by_labels::<Deployment>(&kube_client, "env=example").await.unwrap();
///     let secret = find_secret_ref_in_deployment(&deployment, |env_var| env_var.name == "PASSWORD");
/// };
/// ```
pub fn find_secret_ref_in_deployment(
    deployment: &Deployment,
    condition: impl FnMut(&&EnvVar) -> bool,
) -> Result<SecretKeySelector, Error> {
    let all_containers = get_all_containers_in_deployment(deployment)?;

    let mut all_env_vars = all_containers
        .into_iter()
        .filter_map(|container| container.env.as_ref())
        .flatten();

    let matching_env_var = all_env_vars
        .find(condition)
        .cloned()
        .ok_or(Error::DataNotFound)?;

    matching_env_var
        .value_from
        .ok_or(Error::FieldNotFound(KubeFields::EnvVarSource))?
        .secret_key_ref
        .ok_or(Error::FieldNotFound(KubeFields::SecretKeySelector))
}

/// Based on a secret ref provided within a Deployment, load the value of the secret.
pub async fn load_secret_value_through_deployment(
    kube_client: &Client,
    deployment_labels: impl AsRef<str> + Send,
    secret_name: impl AsRef<str> + Send,
) -> Result<SecretString, Error> {
    let deployment = search_by_labels::<Deployment>(kube_client, deployment_labels).await?;

    let password_secret_ref =
        find_secret_ref_in_deployment(&deployment, |e| e.name == secret_name.as_ref())?;

    let password_secret_namespace = deployment
        .metadata
        .namespace
        .ok_or(Error::FieldNotFound(KubeFields::DeploymentSpec))?;

    let password_secret_name = password_secret_ref
        .name
        .ok_or(Error::FieldNotFound(KubeFields::SecretKeySelector))?;
    let password_secret_key = &password_secret_ref.key;

    load_value_from_secret(
        kube_client,
        &password_secret_namespace,
        &password_secret_name,
        password_secret_key,
    )
    .await
}

/// Load the IP of the Kubernetes cluster from a provided Kubernetes Configuration.
pub fn get_system_ip(kube_config: &Config) -> Result<String, Error> {
    let kube_authority = kube_config
        .cluster_url
        .authority()
        .ok_or(Error::FieldNotFound(KubeFields::Authority))?;
    Ok(kube_authority.host().to_string())
}

/// Searches for a service by labels, then returns an address to the externally available service port.
/// This assumes that the first service that is found with the labels is of type `NodePort`.
//  #lizard forgives the complexity
async fn load_service_external_host(
    kube_config: &Config,
    kube_client: &Client,
    labels: impl AsRef<str> + Send,
) -> Result<String, Error> {
    let service = search_by_labels::<Service>(kube_client, labels).await?;
    let service_nodeport = service
        .spec
        .ok_or(Error::FieldNotFound(KubeFields::ServiceSpec))?
        .ports
        .ok_or(Error::FieldNotFound(KubeFields::ServicePort))?
        .first()
        .ok_or(Error::DataNotFound)?
        .node_port
        .ok_or(Error::FieldNotFound(KubeFields::NodePort))?;

    let system_ip = get_system_ip(kube_config)?;

    Ok(format!("{system_ip}:{service_nodeport}"))
}

/// Searches for a service by labels, then returns an address to the internal service port.
// #lizard forgives the complexity
async fn load_service_internal_host(
    kube_client: &Client,
    labels: impl AsRef<str> + Send,
) -> Result<String, Error> {
    let service = search_by_labels::<Service>(kube_client, labels).await?;

    let service_spec = service
        .spec
        .ok_or(Error::FieldNotFound(KubeFields::ServiceSpec))?;
    let service_cluster_ip = service_spec.cluster_ip.ok_or(Error::DataNotFound)?;

    let service_port = service_spec
        .ports
        .ok_or(Error::FieldNotFound(KubeFields::ServicePort))?
        .first()
        .ok_or(Error::DataNotFound)?
        .port;

    Ok(format!("{service_cluster_ip}:{service_port}"))
}

/// Loads the internally or externally available address of the service, depending on whether this function is called from inside or outside the Kubernetes cluster.
pub async fn load_service_host(
    kube_config: &Config,
    kube_client: &Client,
    labels: impl AsRef<str> + Send,
) -> Result<String, Error> {
    if is_inside_cluster(kube_config) {
        load_service_internal_host(kube_client, labels).await
    } else {
        load_service_external_host(kube_config, kube_client, labels).await
    }
}

/// Determine whether this program is running from inside the cluster that is referred to by the provided Kubernetes Configuration.
fn is_inside_cluster(kube_config: &Config) -> bool {
    if let Ok(cluster_host) = std::env::var("KUBERNETES_SERVICE_HOST") {
        let system_ip = get_system_ip(kube_config).unwrap();
        tracing::debug!("Found system ip: {system_ip}, environment variable KUBERNETES_SERVICE_HOST: {cluster_host}");
        cluster_host == get_system_ip(kube_config).unwrap()
    } else {
        tracing::debug!("Did not find KUBERNETES_SERVICE_HOST environment variable, using external service address");
        false
    }
}

use std::fmt;

/// A helper struct for handling errors within this module.
pub enum Error {
    /// Represents an error when a specific field is not found in the KubeFields enum.
    FieldNotFound(KubeFields),
    /// Represents an error when data is not found.
    DataNotFound,
    /// Represents an error specific to the kube crate.
    KubeError(kube::Error),
    /// Represents an error when a FromUtf8Error occurs.
    FromUtf8Error(FromUtf8Error),
    /// Represents an error when a search operation fails.
    SearchFailed,
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::FieldNotFound(ref field) => write!(f, "Field not found: {field:?}"),
            Self::DataNotFound => write!(f, "Data not found"),
            Self::KubeError(ref err) => write!(f, "Kubernetes error: {err}"),
            Self::FromUtf8Error(ref err) => write!(f, "UTF-8 conversion error: {err}"),
            Self::SearchFailed => write!(f, "Search failed"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Self::KubeError(ref err) => Some(err),
            Self::FromUtf8Error(ref err) => Some(err),
            _ => None,
        }
    }
}

impl From<kube::Error> for Error {
    fn from(err: kube::Error) -> Self {
        Self::KubeError(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Self {
        Self::FromUtf8Error(err)
    }
}

/// Fields within a Kubernetes manifest.
#[derive(Debug, Copy, Clone)]
pub enum KubeFields {
    /// Represents the pod.spec field within a Kubernetes manifest.
    PodSpec,
    /// Represents the deployment.spec field within a Kubernetes manifest.
    DeploymentSpec,
    /// Represents the Service field within a Kubernetes manifest.
    Service,
    /// Represents the EnvVarSource field within a Kubernetes manifest.
    EnvVarSource,
    /// Represents the SecretKeySelector field within a Kubernetes manifest.
    SecretKeySelector,
    /// Represents the Authority field within a Kubernetes manifest.
    Authority,
    /// Represents the service.spec field within a Kubernetes manifest.
    ServiceSpec,
    /// Represents the ServicePort field within a Kubernetes service manifest.
    ServicePort,
    /// Represents the NodePort field within a Kubernetes service manifest.
    NodePort,
}
