//! Tools that make writing tests for this crate faster.
#![allow(clippy::missing_panics_doc)]

use kube::{Client, Config};
use redis::Commands;
use secrecy::SecretString;

use crate::test_helpers_kube::*;
use crate::{
    controller::{ChatMonitor, MsgBroker},
    controller_config::ControllerConfig,
    redis::{Redis, RedisConfig},
    weechat_relay::{WeechatRelay, WeechatRelayConfig},
};

/// Labels for the Redis Twitch Observer in the CI environment.
pub const REDIS_LABELS: &str = "app=redis-twitch-observer,environment=ci";
/// Name for the Redis password environment variable.
pub const REDIS_PASSWORD_ENV_VAR: &str = "REDIS_PASSWORD";
/// Labels for the Weechat app in the CI environment.
pub const WEECHAT_LABELS: &str = "app=weechat-monitor,environment=ci";
/// Name for the Weechat Relay Password environment variable.
pub const WEECHAT_PASSWORD_ENV_VAR: &str = "RELAY_PASSWORD";

/// Name of the redis channel that contains the list of tracked channels
pub const REDIS_TRACKED_CHANNELS_SET: &str = "tracked_channels";

/// Load the Weechat test host from Kubernetes configuration.
pub async fn load_weechat_test_host() -> Result<String, Box<dyn std::error::Error>> {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone())?;

    Ok(load_service_host(&kube_config, &kube_client, WEECHAT_LABELS).await?)
}

/// Load the Weechat test password from Kubernetes configuration.
pub async fn load_weechat_test_password() -> Result<SecretString, Box<dyn std::error::Error>> {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone())?;

    Ok(
        load_secret_value_through_deployment(
            &kube_client,
            WEECHAT_LABELS,
            WEECHAT_PASSWORD_ENV_VAR,
        )
        .await?,
    )
}

/// Load the configuration to access a test instance of Weechat.
pub async fn load_weechat_test_config() -> WeechatRelayConfig {
    let weechat_relay_host = load_weechat_test_host().await.unwrap();
    let weechat_relay_password = load_weechat_test_password().await.unwrap();

    WeechatRelayConfig {
        weechat_relay_host,
        weechat_relay_password,
    }
}

/// Establish a connection to a test instance of Weechat Relay.
pub async fn establish_relay_test_conn() -> WeechatRelay {
    let weechat_relay_config = load_weechat_test_config().await;
    tracing::info!(
        "Establishing connection to Weechat Relay at {weechat_relay_host}...",
        weechat_relay_host = weechat_relay_config.weechat_relay_host,
    );

    WeechatRelay::connect(&weechat_relay_config).unwrap()
}

/// Establish a connection to a test instance of Redis.
pub async fn establish_redis_test_conn() -> Redis {
    let redis_config = load_redis_test_config().await;
    tracing::info!(
        "Establishing connection to Redis at {redis_host} with set {redis_set_name}...",
        redis_host = redis_config.redis_host,
        redis_set_name = redis_config.redis_set_name
    );

    Redis::connect(&redis_config).await.unwrap()
}

/// Load the Redis test configuration, and creates a [`RedisConfig`] item based on it.
pub async fn load_redis_test_config() -> RedisConfig {
    let (redis_host, redis_password) = load_redis_conn_config().await.unwrap();
    RedisConfig {
        redis_host,
        redis_password,
        redis_set_name: REDIS_TRACKED_CHANNELS_SET.to_string(),
    }
}

impl Redis {
    /// Clear all data from the set containing tracked channels.
    pub fn clear_channel_set(&mut self) -> Result<(), redis::RedisError> {
        self.connection
            .request_connection
            .del(REDIS_TRACKED_CHANNELS_SET)
    }

    /// Add a channel to the list of tracked channels.
    pub fn add_channel(&mut self, channel: impl AsRef<str>) -> Result<(), redis::RedisError> {
        self.connection
            .request_connection
            .sadd(REDIS_TRACKED_CHANNELS_SET, channel.as_ref())
    }

    /// Remove a channel from the list of tracked channels.
    pub fn remove_channel(&mut self, channel: impl AsRef<str>) -> Result<(), redis::RedisError> {
        self.connection
            .request_connection
            .srem(REDIS_TRACKED_CHANNELS_SET, channel.as_ref())
    }
}

#[allow(clippy::significant_drop_tightening)] // false positive
/// Loads the Redis connection configuration from the Kubernetes cluster.
pub async fn load_redis_conn_config() -> Result<(String, SecretString), Box<dyn std::error::Error>>
{
    let kube_config = Config::infer().await.unwrap();

    let kube_client = Client::try_from(kube_config.clone())?;

    let password =
        load_secret_value_through_deployment(&kube_client, REDIS_LABELS, REDIS_PASSWORD_ENV_VAR)
            .await?;

    // Host is of the format "{cluster_ip}::{nodeport}"
    let redis_host = load_service_host(&kube_config, &kube_client, REDIS_LABELS).await?;

    Ok((redis_host, password))
}

impl ControllerConfig<WeechatRelay, Redis> {
    /// Loads an entire configuration set to run an instance of the controller that is connected to the test deployments in the Kubernetes cluster.
    pub async fn load_test_config() -> Self {
        use crate::test_helpers::*;

        let weechat_relay_config = load_weechat_test_config().await;
        let redis_config = load_redis_test_config().await;

        Self {
            monitor_config: weechat_relay_config,
            broker_config: redis_config,
        }
    }
}
