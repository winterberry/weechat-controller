//! This module contains the interface in which all parts of this program come together:
//! Traits for Chat Monitor and Message Broker are defined, and based on them, a main loop function is declared.

use std::{collections::HashSet, error::Error, fmt::Debug};

use either::Either;
use tracing::{info, instrument};

/// Describes structures that implement an IRC Chat Monitor.
pub trait ChatMonitor: Sized + Send + Sync {
    /// Configuration to log into the chat monitor interface.
    type Config: MonitorConfig;
    /// Connection to the chat monitor.
    type Connection: MonitorConnection;
    /// Any Error that is returned by the chat monitor.
    type Error: Error + 'static;

    /// Connects the chat monitor using the provided configuration. After this function is called, calls to the Chat Monitor interface should be possible without further preparation.
    fn connect(config: &Self::Config) -> Result<Self, Self::Error>;
    /// Compares the currently joined channels with the channels listed in the provided message broker.
    fn compare_channels_with_broker<MB: MsgBroker>(
        &mut self,
        msg_broker: &mut MB,
    ) -> Result<(HashSet<String>, HashSet<String>), Self::Error>;
    /// Joins and Parts channels so that the channels that are being monitored are the same as the ones listed in the message broker system.
    fn align_channels_with_broker<MB: MsgBroker>(
        &mut self,
        msg_broker: &mut MB,
    ) -> Result<(), Self::Error>;
}

/// Describes message broker systems that provide information about which IRC channels are to be joined.
#[async_trait::async_trait]
pub trait MsgBroker: Sized + Send + Sync {
    /// Configuration to log into the message broker system.
    type Config: BrokerConfig;
    /// Connection to the message broker.
    type Connection: BrokerConnection;
    /// Message that is being received through the broker.
    type Message: Debug;
    /// Any Error that is returned by the message broker.
    type Error: Error + 'static;

    /// Connects the message broker using the provided configuration.
    async fn connect(config: &Self::Config) -> Result<Self, Self::Error>;
    /// Subscribes to notifications from the message broker.
    async fn subscribe_to_notifications(&mut self) -> Result<(), Self::Error>;
    /// Waits until a message is received through the PubSub channel.
    async fn notification_stream(&mut self) -> Option<Self::Message>;
    /// Receive the list of channels that are to be joined from the message broker.
    fn get_channels(&mut self) -> HashSet<String>;
}
/// A trait representing the necessary configuration for a chat monitor.
pub trait MonitorConfig: Send + Sync {}

/// A trait representing the necessary configuration for a broker.
pub trait BrokerConfig: Send + Sync {}

/// A trait representing a connection to a chat monitor.
pub trait MonitorConnection {}

/// A trait representing a connection to a broker.
pub trait BrokerConnection {}

/// A trait representing a configuration for a controller that abstracts over a chat monitor and a broker.
pub trait AbstractControllerConfig<CM, MB>: Send + Sync
where
    CM: ChatMonitor,
    MB: MsgBroker,
{
    /// Returns the configuration for the chat monitor.
    fn monitor_config(&self) -> &CM::Config;
    /// Returns the configuration for the broker.
    fn broker_config(&self) -> &MB::Config;
}

/// Launches a controller given a configuration. The controller will connect to the chat monitor and the broker.
/// The chat monitor is responsible for joining and parting IRC channels based on the information from the broker.
/// The broker provides the list of channels that need to be joined.
///
/// The function takes as argument a configuration that implements the [`AbstractControllerConfig`] trait.
/// This configuration provides the necessary information for the chat monitor and the broker to establish a connection.
///
/// Once the connections are established, the controller will align the channels with the broker.
/// This means that the chat monitor will join or part channels so that the channels it is monitoring
/// are the same as the ones listed in the broker.
///
/// After the initial alignment, the function enters a loop where it waits for a notification from the broker.
/// Upon receiving a notification, the controller will again align the channels with the broker.
/// This process continues indefinitely, ensuring that the chat monitor is always monitoring the correct channels.
///
/// # Errors
/// The function will return an error if there is a problem connecting to the chat monitor or the broker,
/// or if there is a problem aligning the channels.
//  #lizard forgives the complexity
#[instrument(skip(config))]
pub async fn controller_main_loop<CM, MB>(
    config: impl AbstractControllerConfig<CM, MB>,
) -> Result<(), Either<CM::Error, MB::Error>>
where
    CM: ChatMonitor,
    MB: MsgBroker,
{
    // TODO: If you restart weechat without restarting weechat-controller, weechat won't be in any channels.
    let mut msg_broker = MB::connect(config.broker_config())
        .await
        .map_err(Either::Right)?;
    // TODO: When subscribed, some sort of subscription handle should be returned
    msg_broker
        .subscribe_to_notifications()
        .await
        .map_err(Either::Right)?;

    let mut chat_monitor = CM::connect(config.monitor_config()).map_err(Either::Left)?;

    chat_monitor
        .align_channels_with_broker(&mut msg_broker)
        .map_err(Either::Left)?;
    // TODO: There should be a warning when trying to join a channel without leading # sign
    info!("Entering main loop: Waiting for notifications");
    while let Some(msg) = msg_broker.notification_stream().await {
        info!("Received message: {msg:?}");
        chat_monitor
            .align_channels_with_broker(&mut msg_broker)
            .map_err(Either::Left)?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{test_helpers::*, TINY};
    use ::redis::Commands;
    use map_macro::hash_set;
    use std::time::Duration;
    use tokio::time::sleep;

    /// This test checks the function `compare_channels_with_broker` by setting up a simulated Redis server and Weechat Relay.
    /// It first clears all channels from Redis and parts all channels from the Relay. Then, it adds channels to Redis and joins some to the Relay.
    /// The test then runs the `compare_channels_with_broker` function and checks if the channels to be parted and joined are as expected.
    //  #lizard forgives the complexity
    #[rstest::rstest]
    #[test_log::test(tokio::test)]
    #[timeout(Duration::from_secs(15))]
    async fn test_compare_redis_and_weechat() -> Result<(), Box<dyn Error>> {
        let mut redis = establish_redis_test_conn().await;
        let mut relay = establish_relay_test_conn().await;

        redis
            .connection
            .request_connection
            .del(REDIS_TRACKED_CHANNELS_SET)?;

        relay.part_all_twitch_channels()?;
        while !relay.get_open_twitch_channels()?.is_empty() {
            sleep(TINY).await;
        }

        // Add to redis set tracked_channels: [#xqc, #twitch]
        redis
            .connection
            .request_connection
            .sadd(REDIS_TRACKED_CHANNELS_SET, vec!["#xqc", "#twitch"])?;

        // Join the following channels through weechat relay: [#shroud, #twitch, #lydiaviolet]
        relay.send_join("#shroud")?;
        relay.send_join("#twitch")?;
        relay.send_join("#lydiaviolet")?;

        loop {
            // Run compare_redis_and_weechat function
            let (channels_to_be_parted, channels_to_be_joined) =
                relay.compare_channels_with_broker(&mut redis)?;

            // Check if channels_to_be_parted is [#shroud,#lydiaviolet] and channels_to_be_joined is [#xqc].
            if channels_to_be_parted
                == hash_set! {"#shroud".to_string(), "#lydiaviolet".to_string()}
                && channels_to_be_joined == hash_set! {"#xqc".to_string()}
            {
                break;
            }

            sleep(TINY).await;
        }
        Ok(())
    }
}
