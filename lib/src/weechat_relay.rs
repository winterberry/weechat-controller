//! Implementations that allow us to use WeeChat as a Chat Monitor.
//! Communication with WeeChat is achieved by using the Weechat Relay system.

use std::collections::HashSet;

use crate::controller::{ChatMonitor, MonitorConfig, MonitorConnection, MsgBroker};
use crate::relay_error::RelayError;
use once_cell::sync::Lazy;
use regex::Regex;
use secrecy::SecretString;
use std::fmt;
use tracing::instrument;
use tracing::{debug, info};
use weechat_relay_rs::commands::{Count, InitCommand, PointerOrName};
use weechat_relay_rs::messages::{Message, WString};
use weechat_relay_rs::messages::{Object, WArray};
use weechat_relay_rs::{
    commands::{Command, Countable, HdataCommand, InputCommand, StringArgument},
    Connection,
};

/// Configuration to connect to a Weechat Relay.
#[derive(Clone, Debug)]
pub struct WeechatRelayConfig {
    /// The host of the Weechat Relay.
    pub weechat_relay_host: String,
    /// The password for the Weechat Relay.
    pub weechat_relay_password: SecretString,
}

/// Connection to a Weechat Relay.
#[allow(missing_debug_implementations)]
pub struct WeechatRelayConnection(weechat_relay_rs::Connection);

/// Used to interact with the `WeeChat` Relay interface.
pub struct WeechatRelay {
    /// The connection to the Weechat Relay
    pub connection: weechat_relay_rs::Connection,
    /// The configuration for the Weechat Relay
    _config: WeechatRelayConfig,
}

impl MonitorConfig for WeechatRelayConfig {}
impl MonitorConnection for WeechatRelayConnection {}

impl ChatMonitor for WeechatRelay {
    type Config = WeechatRelayConfig;
    type Connection = WeechatRelayConnection;
    type Error = RelayError;

    #[instrument]
    fn connect(config: &Self::Config) -> Result<Self, Self::Error> {
        let connection = Self::connect_and_init(
            config.weechat_relay_host.clone(),
            &config.weechat_relay_password,
        )?;

        info!("Successfully initialized connection to Weechat Relay");
        Ok(Self {
            connection,
            _config: config.clone(),
        })
    }

    #[instrument(skip(self, msg_broker))]
    fn compare_channels_with_broker<MB: MsgBroker>(
        &mut self,
        msg_broker: &mut MB,
    ) -> Result<(HashSet<String>, HashSet<String>), Self::Error> {
        let weechat_channels: HashSet<String> =
            self.get_open_twitch_channels()?.into_iter().collect();
        debug!("Weechat channels: {weechat_channels:?}");

        let redis_channels: HashSet<String> = msg_broker.get_channels();
        debug!("Redis channels: {redis_channels:?}");

        let channels_to_be_parted: HashSet<String> = weechat_channels
            .difference(&redis_channels)
            .cloned()
            .collect();
        debug!("Channels to be parted: {channels_to_be_parted:?}");

        let channels_to_be_joined: HashSet<String> = redis_channels
            .difference(&weechat_channels)
            .cloned()
            .collect();
        debug!("Channels to be joined: {channels_to_be_joined:?}");

        // TODO: Instead of returning a tuple, I could create an enum with fields Join() and Part()

        Ok((channels_to_be_parted, channels_to_be_joined))
    }

    // #lizard forgives the complexity
    #[instrument(skip(self, msg_broker))]
    fn align_channels_with_broker<MB: MsgBroker>(
        &mut self,
        msg_broker: &mut MB,
    ) -> Result<(), Self::Error> {
        info!("Running alignment between Redis Data and Weechat Channels");
        let (channels_to_be_parted, channels_to_be_joined) =
            self.compare_channels_with_broker(msg_broker)?;

        for c in channels_to_be_parted {
            self.send_part(c)?;
        }

        for c in channels_to_be_joined {
            self.send_join(c)?;
        }

        Ok(())
    }
}

impl WeechatRelay {
    /// Establishes a connection to the `WeeChat` Relay interface and initializes the connection.
    ///
    /// # Errors
    ///
    /// This function will return an error if the TCP stream cannot be established, or if the initialization command cannot be sent to the `WeeChat` Relay.
    pub fn connect_and_init(
        address: impl AsRef<str>,
        password: &SecretString,
    ) -> Result<Connection, RelayError> {
        use secrecy::ExposeSecret;
        let stream = std::net::TcpStream::connect(address.as_ref())?;
        let mut connection = Connection { stream };
        let init_command = InitCommand::new(
            Some(StringArgument::new(password.expose_secret().into())?),
            None,
            None,
        );
        connection.send_command(&Command::new(None, init_command))?;

        // TODO: Test if Relay configuration is as expected

        Ok(connection)
    }

    /// Sends an `input` command to the specified target buffer on the `WeeChat` Relay interface.
    #[allow(clippy::cognitive_complexity)]
    pub fn send_input(
        &mut self,
        target_buffer: impl Into<String>,
        command: impl Into<String>,
    ) -> Result<(), RelayError> {
        let target_buffer = target_buffer.into();
        let command = command.into();
        debug!("Sending command {command} to target buffer {target_buffer}");

        let input_command = {
            let target_buffer_arg = PointerOrName::Name(StringArgument::new(target_buffer)?);
            let command_arg = StringArgument::new(command)?;
            InputCommand::new(target_buffer_arg, command_arg)
        };

        Ok(self
            .connection
            .send_command(&Command::new(None, input_command))?)
    }

    /// Sends an IRC JOIN command to the Twitch server through the `WeeChat` Relay.
    #[allow(clippy::cognitive_complexity)]
    pub fn send_join(&mut self, channel: impl AsRef<str>) -> Result<(), RelayError> {
        let channel = channel.as_ref();

        info!("Sending join command to channel: {channel}");
        let command = format!("/join {channel}");
        self.send_input("irc.server.twitch", command)?;
        info!("Successfully sent join command to channel: {channel}");

        Ok(())
    }

    /// Sends an IRC PART command to a twitch channel through the `WeeChat` Relay.
    #[allow(clippy::cognitive_complexity)]
    pub fn send_part(&mut self, channel: impl AsRef<str>) -> Result<(), RelayError> {
        let channel = channel.as_ref();

        info!("Sending part command to channel: {channel}");
        let target_buffer = format!("irc.twitch.{channel}");
        self.send_input(target_buffer, "/part")?;
        info!("Successfully sent part command to channel: {channel}");

        Ok(())
    }

    /// Send a command to part from all channels in the Twitch server.
    /// This sends the `WeeChat` command `/allchan /part` to the buffer `irc.server.twitch`, which `WeeChat` will then translate into invdividual `PART` commands and execute.
    /// Note that if `WeeChat` anti-flooding features are enabled (which they are by default), this can take quite a while, since a waiting period is introduced between each `PART` command.
    /// To turn off anti-flood, set `irc.server.{server_name}.anti_flood_prio_low` and `irc.server.{server_name}.anti_flood_prio_high` to `0`.
    pub fn part_all_twitch_channels(&mut self) -> Result<(), RelayError> {
        self.send_input("irc.server.twitch", "/allchan /part")
    }

    /// Sends an `hdata` command to the specified target buffer on the `WeeChat` Relay interface and retrieves its output message.
    ///
    /// The `hdata` command is used to access `WeeChat`'s internal data structures. It allows you to retrieve information about various aspects of `WeeChat`'s state, such as the list of buffers, the list of channels, or the list of nicknames in a channel.
    ///
    /// It is a powerful command that requires a good understanding of `WeeChat`'s internal data structures. Misuse of the `hdata` command can lead to unexpected results or even crash `WeeChat`. Therefore, it should be used with caution and only when necessary.
    pub fn get_hdata(&mut self, hdata_command: HdataCommand) -> Result<Message, RelayError> {
        let hdata_message = {
            self.connection
                .send_command(&Command::new(None, hdata_command))?;
            self.connection.get_message()?
        };
        debug!("Successfully got hdata: {hdata_message:?}");

        Ok(hdata_message)
    }

    /// Use the `WeeChat` hdata command to obtain the list of open buffers.
    /// The following command is being sent to `WeeChat` Relay:
    /// ```bash
    /// hdata buffer:gui_buffers(*) full_name
    /// ```
    /// This function returns the raw Weechat Relay response from which the values can then be extracted.
    fn get_open_buffer_hdata(&mut self) -> Result<weechat_relay_rs::messages::Message, RelayError> {
        let hdata_command = {
            let hdata_name = StringArgument::new(String::from("buffer"))?;
            let hdata_pointer = Countable::new(
                Some(Count::Glob),
                PointerOrName::Name(StringArgument::new(String::from("gui_buffers"))?),
            );
            let hdata_keys = [StringArgument::new(String::from("full_name"))?].to_vec();
            HdataCommand::new(hdata_name, hdata_pointer, vec![], hdata_keys)
        };
        let hdata_message = self.get_hdata(hdata_command)?;
        Ok(hdata_message)
    }

    /// Try to decode a [`WString`] object into a [`String`].
    fn decode_wstring(wstring: &WString) -> Result<String, RelayError> {
        let data = wstring
            .bytes()
            .as_ref()
            .ok_or(RelayError::DecodeHDataError)?;
        let decoded = String::from_utf8(data.clone())?;

        Ok(decoded)
    }

    /// Destructure an `hdata` [`Message`] object to obtain the contained list of buffers.
    fn take_buffers_from_hdata(
        hdata: &weechat_relay_rs::messages::Message,
    ) -> Result<&Vec<WString>, RelayError> {
        let Object::Hda(inner_message) = &hdata.objects[0] else {
            return Err(RelayError::DecodeHDataError);
        };
        let WArray::Str(buffers_wstring) = &inner_message.set_values[0].values else {
            return Err(RelayError::DecodeHDataError);
        };

        Ok(buffers_wstring)
    }

    /// Use the `WeeChat` hdata command to obtain the list of open buffers.
    /// The following command is being sent to `WeeChat` Relay:
    /// ```bash
    /// hdata buffer:gui_buffers(*) full_name
    /// ```
    /// A sample result could be ´["core.weechat", "irc.server.libera", "irc.libera.#weechat"]´.
    pub fn get_open_buffers(&mut self) -> Result<HashSet<String>, RelayError> {
        let hdata = self.get_open_buffer_hdata()?;
        let buffers_wstring = Self::take_buffers_from_hdata(&hdata)?;

        let open_buffers = buffers_wstring
            .iter()
            .map(Self::decode_wstring)
            .collect::<Result<HashSet<_>, _>>()?;

        debug!("Obtained open buffers from Weechat Relay: {open_buffers:?}");

        Ok(open_buffers)
    }

    /// From the name of a WeeChat buffer, extracts which Twitch chnannel it belongs to, if it belongs to any.
    /// For example, for `irc.twitch.#example`, this will extract `#example`.
    fn extract_channel_name_from_buffer_name(buffer_name: impl AsRef<str>) -> Option<String> {
        static CHANNEL_FROM_BUFFER_REGEX: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"irc\.twitch\.(?<channel_name>#.+)").unwrap());

        CHANNEL_FROM_BUFFER_REGEX
            .captures(buffer_name.as_ref())
            .map(|captures| captures["channel_name"].to_string())
    }

    /// Retrieve a list of all Twitch channels that are opened on `WeeChat`.
    /// If `WeeChat` is configured to close any buffer when parting from a channel, this is equivalent to the list of currently joined channels.
    /// To set `WeeChat` up to close on part, set `irc.look.part_closes_buffer` to `on`.
    pub fn get_open_twitch_channels(&mut self) -> Result<HashSet<String>, RelayError> {
        let open_buffers = self.get_open_buffers()?;
        let open_channels: HashSet<String> = open_buffers
            .iter()
            .filter_map(Self::extract_channel_name_from_buffer_name)
            .collect();

        debug!("Obtained open channels from Weechat Relay: {open_channels:?}");
        Ok(open_channels)
    }
}

impl fmt::Debug for WeechatRelay {
    #[allow(clippy::used_underscore_binding)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("WeechatRelay")
            .field("_config", &self._config)
            .finish_non_exhaustive()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use map_macro::hash_set;
    use tokio::time::sleep;

    use super::*;
    use crate::{test_helpers::*, TINY};

    #[test_log::test(tokio::test)]
    async fn connect_to_relay() {
        let _ = establish_relay_test_conn().await;
    }

    // #lizard forgives the complexity
    #[rstest::rstest]
    #[test_log::test(tokio::test)]
    #[timeout(Duration::from_secs(15))]
    async fn part_all_channels() -> Result<(), Box<dyn std::error::Error>> {
        let mut relay = establish_relay_test_conn().await;

        relay.part_all_twitch_channels()?;
        while !relay.get_open_twitch_channels()?.is_empty() {
            sleep(TINY).await;
        }

        relay.send_join("#twitch")?;
        while relay.get_open_twitch_channels()? != hash_set![String::from("#twitch")] {
            sleep(TINY).await;
        }

        relay.part_all_twitch_channels()?;
        while !relay.get_open_twitch_channels()?.is_empty() {
            sleep(TINY).await;
        }

        Ok(())
    }

    #[rstest::rstest]
    #[test_log::test(tokio::test)]
    #[timeout(Duration::from_secs(15))]
    // #lizard forgives the complexity
    async fn join_channel() -> Result<(), Box<dyn std::error::Error>> {
        let mut relay = establish_relay_test_conn().await;

        relay.part_all_twitch_channels()?;
        while !relay.get_open_twitch_channels()?.is_empty() {
            sleep(TINY).await;
        }

        relay.send_join("#twitch")?;
        while relay.get_open_twitch_channels()? != hash_set![String::from("#twitch")] {
            sleep(TINY).await;
        }

        Ok(())
    }

    #[test]
    fn extract_channel_name_from_buffer_name() {
        assert_eq!(
            WeechatRelay::extract_channel_name_from_buffer_name("irc.server.weechat"),
            None
        );
        assert_eq!(
            WeechatRelay::extract_channel_name_from_buffer_name("irc.twitch.#example"),
            Some("#example".to_string())
        );
    }
}
