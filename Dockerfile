FROM debian:bookworm-slim

RUN echo "Running on $(uname -m)"

RUN apt-get update \
    && apt-get install -y libssl-dev ca-certificates wget \
    && rm -rf /var/lib/apt/lists/*

COPY ./weechat-controller-cli /bin/weechat-controller-cli

ENTRYPOINT ["weechat-controller-cli"]
