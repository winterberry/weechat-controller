use elasticsearch::{
    auth::Credentials,
    cert::CertificateValidation,
    http::transport::{SingleNodeConnectionPool, TransportBuilder},
    indices::IndicesStatsParts,
    Elasticsearch,
};
use k8s_openapi::api::{
    apps::v1::StatefulSet,
    core::v1::{ConfigMap, Pod},
};
use kube::{api::DeleteParams, Api, Client, Config};
use kube_discovery::LabelSelector;
use redis::Commands;
use secrecy::ExposeSecret;
use std::time::Duration;
use tokio::time::sleep;
use url::Url;

const CHANNELS_TO_JOIN: [&str; 17] = [
    "#ninja",
    "#auronplay",
    "#ibai",
    "#rubius",
    "#xqc",
    "#thegrefg",
    "#tfue",
    "#juansguarnizo",
    "#shroud",
    "#pokimane",
    "#sodapoppin",
    "#elmariana",
    "#elspreen",
    "#heelmike",
    "#tommyinnit",
    "#adinross",
    "#kaicenat",
];
const ELASTICSEARCH_LABELS: &str = "app=chatmsg-elasticsearch,environment=staging";
const MONITOR_LABELS: &str = "app=weechat-monitor,environment=staging";
const REDIS_LABELS: &str = "app=redis-twitch-observer,environment=staging";
const CONTROLLER_CONFIG_LABELS: &str = "app=weechat-controller,config=config,environment=staging";

/// Name of the environment variable containing the password in the Redis deployment
const REDIS_PASSWORD_ENV_VAR: &str = "REDIS_PASSWORD";

/// We test if new messages are still being logged after restarting the Weechat Monitor.
/// This is to ensure that channels are automatically re-joined when the monitor is restarted.
#[test_log::test(tokio::test)]
async fn restart_weechat_monitor() {
    tracing::info!("Starting test");
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    // we need to join a bunch of big channels so that we actually get a perceptible amount of messages during the test
    let redis_url = {
        let redis_host = LabelSelector(REDIS_LABELS)
            .load_service_host(&kube_config, &kube_client)
            .await
            .unwrap();

        let redis_password = LabelSelector(REDIS_LABELS)
            .load_secret_value_through_workload(&kube_client, REDIS_PASSWORD_ENV_VAR)
            .await
            .unwrap()
            .expose_secret()
            .clone();
        let redis_password_url = urlencoding::encode(&redis_password);

        tracing::info!("Connecting to redis on {redis_host}");
        format!("redis://:{redis_password_url}@{redis_host}")
    };

    let mut redis_connection = redis::Client::open(redis_url)
        .unwrap()
        .get_connection()
        .unwrap();
    tracing::info!("Connected to redis");

    let redis_tracked_channels_set = {
        let controller_config_map: ConfigMap = LabelSelector(CONTROLLER_CONFIG_LABELS)
            .find_resource(&kube_client)
            .await
            .unwrap();
        controller_config_map
            .data
            .unwrap()
            .get("REDIS_SET_NAME")
            .unwrap()
            .clone()
    };

    for channel in CHANNELS_TO_JOIN {
        let _: () = redis_connection
            .sadd(&redis_tracked_channels_set, channel)
            .unwrap();
    }
    tracing::info!("Joined channels: {CHANNELS_TO_JOIN:?}");

    // wait a bit so that weechat-controller instructs the monitor to join the channels
    sleep(Duration::from_secs(10)).await;

    // we kill the monitor pod
    let monitor_pod: Pod = LabelSelector(MONITOR_LABELS)
        .find_resource(&kube_client)
        .await
        .unwrap();
    let pod_api: Api<Pod> = Api::namespaced(
        kube_client.clone(),
        &monitor_pod.metadata.namespace.unwrap(),
    );
    pod_api
        .delete(
            &monitor_pod.metadata.name.unwrap(),
            &DeleteParams::default(),
        )
        .await
        .unwrap();
    tracing::info!("Killed weechat-monitor pod");

    sleep(Duration::from_secs(3)).await;

    // wait until Weechat Monitor is running again
    while !weechat_is_ready(&kube_client).await {
        sleep(Duration::from_secs(1)).await;
    }
    tracing::info!("Weechat-monitor pod is ready");

    // we get the number of elasticsearch items at the start of the test
    let start_msg_count = get_elasticsearch_item_count().await;
    tracing::info!("Found {start_msg_count} messages at start");

    // we wait a while to collect messages
    sleep(Duration::from_secs(120)).await;

    let end_msg_count = get_elasticsearch_item_count().await;
    tracing::info!("Found {end_msg_count} messages at end");

    let collected_msg_count = end_msg_count - start_msg_count;

    // remove all channels that were previously added
    for channel in CHANNELS_TO_JOIN {
        let _: () = redis_connection
            .srem(&redis_tracked_channels_set, channel)
            .unwrap();
    }
    tracing::info!("Left channels: {CHANNELS_TO_JOIN:?}");

    assert!(collected_msg_count > 1000);
}

/// Weechat is considered ready if there is at least one replica available
async fn weechat_is_ready(kube_client: &Client) -> bool {
    let weechat_statefulset: StatefulSet = LabelSelector(MONITOR_LABELS)
        .find_resource(kube_client)
        .await
        .unwrap();

    let available_replicas = weechat_statefulset
        .status
        .unwrap()
        .available_replicas
        .unwrap();

    available_replicas > 0
}

/// Get the number of items in the elasticsearch index
async fn get_elasticsearch_item_count() -> i64 {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    let elasticsearch_url = {
        let host = LabelSelector(ELASTICSEARCH_LABELS)
            .load_service_host(&kube_config, &kube_client)
            .await
            .unwrap();
        Url::parse(&format!("https://{host}")).unwrap()
    };

    let elasticsearch_credentials = {
        let user = "elastic";
        let password = LabelSelector(MONITOR_LABELS)
            .load_secret_value_through_workload(&kube_client, "ELASTICSEARCH_PASSWORD")
            .await
            .unwrap()
            .expose_secret()
            .clone();
        Credentials::Basic(user.into(), password.clone())
    };

    let elasticsearch_client = {
        let connection_pool = SingleNodeConnectionPool::new(elasticsearch_url);
        let transport = TransportBuilder::new(connection_pool)
            .auth(elasticsearch_credentials)
            .cert_validation(CertificateValidation::None)
            .build()
            .unwrap();
        Elasticsearch::new(transport)
    };

    // get item count
    let indices_response = elasticsearch_client
        .indices()
        .stats(IndicesStatsParts::IndexMetric(&["irc-messages"], &["docs"]))
        .send()
        .await
        .unwrap();

    tracing::debug!("Reading API response body");

    let response_body: serde_json::Value = indices_response.json().await.unwrap();
    tracing::info!("Elasticsearch index stats/docs API response body: {response_body}");

    response_body["_all"]["primaries"]["docs"]["count"]
        .as_number()
        .unwrap()
        .as_i64()
        .unwrap()
}
